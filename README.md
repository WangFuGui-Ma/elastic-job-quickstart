## 首先我们需要了解ElasticJob 是什么？

> ElasticJob是面向互联网生态和海量任务的分布式调度解决方案，由两个相互独立的子项目 `ElasticJob-Lite` 和`ElasticJob-Cloud` 组成。
> 它通过弹性调度、资源管控、以及作业治理的功能，打造一个适用于互联网场景的分布式调度解决方案，并通过开放的架构设计，提供多元化的作业生态。
> 它的各个产品使用统一的作业 API，开发者仅需一次开发，即可随意部署。

## 看看他们两个有什么区别？
![在这里插入图片描述](https://img-blog.csdnimg.cn/0156f51b41ab4a01b1eb82f4f05fa150.png)
我们这次要讲的就是lite版本的

## 我们需要准备的环境：

jdk + zookeeper + maven

## 快速开始
大家也可以直接去我的代码仓库拉下来直接运行：

[https://gitee.com/WangFuGui-Ma/elastic-job-quickstart](https://gitee.com/WangFuGui-Ma/elastic-job-quickstart)

### 第一步，创建一个maven项目并导入jar包

```xml
	<dependencies>
        <dependency>
            <groupId>com.dangdang</groupId>
            <artifactId>elastic-job-lite-core</artifactId>
            <version>2.1.5</version>
        </dependency>
    </dependencies>
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/9e5b92d91d754e47b2516d93d14b2a2e.png)
### 第二步，创建一个myjob类继承simplejob类
![在这里插入图片描述](https://img-blog.csdnimg.cn/624850d36d4348b48f56e05ddba0810c.png)

```java
public class MyJob implements SimpleJob {
    public void execute(ShardingContext shardingContext) {
        System.out.println("定时任务开始执行了"+ new Date());
    }
}
```
### 第三步，创建一个JobDemo启动类
![在这里插入图片描述](https://img-blog.csdnimg.cn/e761d43ae7e34001be07c318a863ebc1.png)

```java
public class JobDemo {
    public static void main(String[] args) {

        new JobScheduler(createRegistryCenter(), createJobConfiguration()).init();
    }

    private static LiteJobConfiguration createJobConfiguration() {
        //创建作业配置
        JobCoreConfiguration demoSimpleJob = JobCoreConfiguration.newBuilder("demoSimpleJob", "0/3 * * * * ?", 1).build();
        SimpleJobConfiguration simpleJobConfiguration = new SimpleJobConfiguration(demoSimpleJob, MyJob.class.getCanonicalName());

        return LiteJobConfiguration.newBuilder(simpleJobConfiguration).build();
    }

    private static CoordinatorRegistryCenter createRegistryCenter() {
        ZookeeperConfiguration zkConfig = new ZookeeperConfiguration("localhost:2181", "elasticJob");
        zkConfig.setSessionTimeoutMilliseconds(100);
        CoordinatorRegistryCenter elasticJob = new ZookeeperRegistryCenter(zkConfig);
        elasticJob.init();
        return elasticJob;
    }

}

```

### 第四步，启动本地的zookeeper

如果不知道怎么启动和安装的话可以参考我的文章：

[https://blog.csdn.net/csdnerM/article/details/121848173](https://blog.csdn.net/csdnerM/article/details/121848173)


### 第五步，启动jobdemo
控制台就会开始打印啦！！恭喜你完成了Elastic-Job的快速入门！！

![在这里插入图片描述](https://img-blog.csdnimg.cn/7751a688c82743e9b1daa9c392b81fd1.png)
