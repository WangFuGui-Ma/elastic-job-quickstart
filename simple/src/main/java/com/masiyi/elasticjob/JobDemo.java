package com.masiyi.elasticjob;

import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.masiyi.elasticjob.job.MyJob;

public class JobDemo {
    public static void main(String[] args) {

        new JobScheduler(createRegistryCenter(), createJobConfiguration()).init();
    }

    private static LiteJobConfiguration createJobConfiguration() {
        //创建作业配置
        JobCoreConfiguration demoSimpleJob = JobCoreConfiguration.newBuilder("demoSimpleJob", "0/3 * * * * ?", 1).build();
        SimpleJobConfiguration simpleJobConfiguration = new SimpleJobConfiguration(demoSimpleJob, MyJob.class.getCanonicalName());

        return LiteJobConfiguration.newBuilder(simpleJobConfiguration).build();
    }

    private static CoordinatorRegistryCenter createRegistryCenter() {
        ZookeeperConfiguration zkConfig = new ZookeeperConfiguration("localhost:2181", "elasticJob");
        zkConfig.setSessionTimeoutMilliseconds(100);
        CoordinatorRegistryCenter elasticJob = new ZookeeperRegistryCenter(zkConfig);
        elasticJob.init();
        return elasticJob;
    }

}
